# frozen_string_literal: true

module IpCam
  class Configuration < WidgetInstanceConfiguration
      attribute :url, :string, default: ""
      attribute :requires_auth, :boolean, default: false
      attribute :reload_stream, :boolean, default: false
      attribute :user, :string, default: ""
      attribute :password, :string, default: ""

      validates :requires_auth, :reload_stream, boolean: true
      validates :url, :user, :password, presence: true, allow_blank: true
  end
end
