# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'ip_cam/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = 'ip_cam'
  s.version = IpCam::VERSION
  s.authors = ['Tobias Grasse']
  s.email = ['tg@glancr.de']
  s.homepage = 'https://glancr.de/modules/ip_cam'
  s.summary = 'mirr.OS widget to display a live IP camera feed.'
  s.description = 'Shows a live video feed from your IP camera.'
  s.license = 'MIT'
  s.metadata = { 'json' =>
                   {
                     type: 'widgets',
                     title: {
                       enGb: 'IP Cam',
                       deDe: 'IP-Kamera',
                       frFr: 'caméra IP',
                       esEs: 'cámara IP',
                       plPl: 'kamera IP',
                       koKr: 'IP 카메라'

                     },
                     description: {
                       enGb: s.description,
                       deDe: 'Zeigt einen Live-Video-Stream deiner IP-Kamera.',
                       frFr: 'Affiche un flux vidéo en direct de votre caméra IP.',
                       esEs: 'Muestra un video en vivo desde su cámara IP.',
                       plPl: 'Wyświetla obraz wideo na żywo z kamery IP.',
                       koKr: 'IP 카메라의 라이브 비디오 피드를 표시합니다.'
                     },
                     sizes: [
                       {
                         w: 6,
                         h: 4
                       },
                       {
                         w: 12,
                         h: 8
                       },
                       {
                         w: 12,
                         h: 21
                       },
                       {
                         w: 21,
                         h: 12
                       }
                     ],
                     languages: [:enGb], # Add snake-cased language tags if your templates have the locales for it.
                     group: nil # see https://gitlab.com/glancr/mirros_one/wikis/home for available groups
                   }.to_json }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
  s.add_development_dependency 'rubocop', '~> 0.81'
  s.add_development_dependency 'rubocop-rails'
end
