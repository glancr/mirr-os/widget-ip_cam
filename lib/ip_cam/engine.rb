# frozen_string_literal: true

module IpCam
  class Engine < ::Rails::Engine
    isolate_namespace IpCam
    config.generators.api_only = true
  end
end
