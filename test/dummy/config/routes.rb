# frozen_string_literal: true

Rails.application.routes.draw do
  mount IpCam::Engine => '/ip_cam'
end
