# frozen_string_literal: true

require 'test_helper'

class IpCam::Test < ActiveSupport::TestCase
  test 'truth' do
    assert_kind_of Module, IpCam
  end
end
